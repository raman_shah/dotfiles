# Path to your oh-my-zsh installation.
export ZSH=/Users/amv146/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="sunaku"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

# User configuration

# Raman Shah, 22 Jan 2016: Anaconda.
export PATH="/Users/amv146/anaconda/bin:$PATH"
# Raman Shah, 13 Apr 2015: Added /usr/texbin to beginning for MacTeX.
# export PATH="/usr/texbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
# Raman Shah, 13 Apr 2015: GNU tools to clobber the mac ones.
export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
export MANPATH="/usr/local/opt/coreutils/libexec/gnuman:/usr/local/man:/usr/share/man"

# Raman Shah, 18 Apr 2016: Anaconda libraries, too
export LD_LIBRARY_PATH="/Users/amv146/anaconda/lib:$LD_LIBRARY_PATH"
export TCL_LIBRARY="/Users/amv146/anaconda/lib/tcl8.5"

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Solarized Light colors for GNU ls
d=~/.dircolors
test -r $d && eval "$(dircolors $d)"

# Raman Shah, 19 Jan 2016: Capital One setup
C1_PROXY=localhost:3128
export http_proxy=${C1_PROXY}
export https_proxy=${C1_PROXY}
export ftp_proxy=${C1_PROXY}
export rsync_proxy=${C1_PROXY}
export HTTP_PROXY=${C1_PROXY}
export HTTPS_PROXY=${C1_PROXY}
export FTP_PROXY=${C1_PROXY}
export RSYNC_PROXY=${C1_PROXY}

# Raman Shah, 21 Jan 2016: vimmifying my shell. Deep breaths.
export EDITOR="vim"
bindkey -v 

# Raman Shah, 21 Jan 2016: Shorten onerous delay between vim modes.
export KEYTIMEOUT=2

# vi style incremental search
bindkey '^R' history-incremental-search-backward
bindkey '^S' history-incremental-search-forward
bindkey '^P' history-search-backward
bindkey '^N' history-search-forward  

# Raman Shah, 20 Jan 2016: bind C-u to delete to beginning of line so
# that we have moves and kills for words forward and backward and
# lines forward and backward. Rationale: editing a line is common, and
# if you're at the end of the command, C-u subsumes this behavior
# anyway.
bindkey "^U" backward-kill-line
