" Notes on vim config:

" Relevant packages installed via Homebrew:
" reattach-to-user-namespace
" the_silver_searcher
" tmux
" macvim --override-system-vim
" zsh

" Tim Pope pathogen package manager

" Bundles in ~/.vim/bundle:
" ag.vim
" rust.vim
" vim-airline
" vim-airline-themes
" vim-colors-solarized
" vim-easy-align
" vim-numbertoggle
" vim-orgmode
" vim-speeddating
" vim-tmux-navigator
" vim-yaml
" vimoutliner
" haskell-vim (https://github.com/neovimhaskell/haskell-vim)

" Pathogen setup
execute pathogen#infect()

" Solarized Light syntax highlighting configuration
syntax enable
set background=light
colorscheme solarized

augroup custom_filetypes
	" Default SAS highlighting is defective.
	au BufNewFile,BufRead *.sas set filetype=txt
augroup END

" Code indentation
filetype indent plugin on
" Overloading a couple opinions for Haskell's sake...
set autoindent
autocmd FileType haskell setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType cabal setlocal shiftwidth=2 tabstop=2 expandtab

" Indent with spaces in html
autocmd FileType html setlocal shiftwidth=2 tabstop=2 expandtab

" vim-numbertoggle gives us a variety of options delimited by C-n.
" Default to absolute line numbering.
set number

" vim-airline configuration.
" Always have the status bar at the bottom.
set laststatus=2
let g:airline#extensions#tabline#enabled = 1

" Tab completion when looking for files: throw me a bone when I'm lost.
set wildmode=longest,list,full
set wildmenu

" Snappier switch between modes
set timeoutlen=1000
set ttimeoutlen=1

" Use system clipboard with natural Vim commands.
set clipboard=unnamed

" Shortcut to remove trailing whitespace
nnoremap <Leader>rtw :%s/\s\+$//e<CR>
