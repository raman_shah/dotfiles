;; Start from Emacs Prelude
;; Install Solarized: M-x package-install RET solarized-theme

(setq prelude-theme 'solarized-light)
;; (setq solarized-high-contrast-mode-line t)
(set-frame-size (selected-frame) 184 93)
(split-window-horizontally)

(add-to-list 'load-path "/Users/raman/ESS/lisp/")
(load "ess-site")

;; Org mode - I like line-wrap for writing Slack standup messages.
(setq org-startup-truncated nil)

;; ESS: We use snake_case a lot, so the underscore to back arrow
;; shortcut is not appreciated.
(ess-toggle-underscore nil) ; leave underscore key alone!
